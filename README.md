# Sxmo Locking for phones

This repository contains the dotfiles needed to add a screenlocker, in my case swaylock mobile to a sxmo >= 1.9.0 mobile desktop, I use it on my pinephone (a touchscreen device with three buttons in sxmo device categories).

## Relevant resources

* [Sxmo Website](https://sxmo.org)
* [Blogpost documenting how the screenlocking part of sxmo works under the hood](https://slatecave.net/blog/2022-05-07_integrating-my-screenlocker-with-sxmo/)
* [Blogpost documenting what I have done and why](https://slatecave.net/blog/2022-05-07_integrating-my-screenlocker-with-sxmo-part-2/)
* [Swaylock-Mobile, the screenlocker I currently use](https://codeberg.org/slatian/swaylock-mobile)

## Files

---
* .config
    * swaylock
        * config - swaylock configuration
    * sxmo
        * hooks
            * pine64,pinephone-1.2 - you may have to rename this one
            * sxmo_hook_idlelocker.sh
            * sxmo_hook_inputhandler.sh
            * sxmo_hook_lock.sh
            * sxmo_hook_proximitylock.sh
            * sxmo_hook_screenoff.sh
            * sxmo_hook_screenon.sh
            * sxmo_hook_unlock.sh
        * profile - nothing modified here
        * sway - added the bindsym --lock here
        * xinit - nothing modified here
    * .gitignore
* .local
    * bin
        * run-screenlocker - script that runs the screenlocker
---

For descriptions of the hooks see the blogposts linked above.
